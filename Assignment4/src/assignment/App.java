package assignment;

public class App {
	public static void main(String[] args) {
		String str = new String("Hi, my name is");
		StringBuilder builder = new StringBuilder(str);
		builder.append(" reza");
		
		str = builder.toString();
		builder.setCharAt(str.indexOf('H'), 'H');
		builder.setCharAt(str.indexOf('m'), 'M');
		builder.setCharAt(str.indexOf('n'), 'N');
		builder.setCharAt(str.indexOf('i'), 'I');
		builder.setCharAt(str.indexOf('r'), 'R');
		
		str = builder.toString();
		System.out.println(builder.toString());
		
		System.out.println(str.indexOf("Reza"));
		
		System.out.println(builder.reverse());
		
		String[] split = str.split(" ");
		for (String s : split) {
			System.out.println(s);
		}
	}
}
