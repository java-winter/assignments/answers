package answer3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String fileName = "src/resources/" + "boats.txt";
		Path path = Paths.get(fileName);

		System.out.println("What is the owner name");
		String ownername = scanner.next();
		System.out.println("What is the size of the boat");
		int size = scanner.nextInt();
		System.out.println("What is the depth of the boat");
		int depth = scanner.nextInt();

		Boat boat = new Boat(ownername, size, depth);

		// Writing to the file
		try (FileWriter fileWriter = new FileWriter(fileName, true)) {
			PrintWriter printer = new PrintWriter(fileWriter);
			printer.println(boat.getOwnerName() + ";" + boat.getSize() + ";" + boat.getDepth());

			List<String> lines = Files.readAllLines(path);
			for (String line : lines) {

				String[] fields = line.split(";");

				String ownerNameFromFile = fields[0];
				int sizeFromFile = Integer.parseInt(fields[1]);
				int depthFromFile = Integer.parseInt(fields[2]);

				Boat boatFromFile = new Boat(ownerNameFromFile, sizeFromFile, depthFromFile);
				System.out.println(boatFromFile);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		// Reading the file
		try {
			List<String> lines = Files.readAllLines(path);
			for (String line : lines) {
				String[] fields = line.split(";");
				String ownerNameFromFile = fields[0];
				int sizeFromFile = Integer.parseInt(fields[1]);
				int depthFromFile = Integer.parseInt(fields[2]);
				Boat b2 = new Boat(ownerNameFromFile, sizeFromFile, depthFromFile);
				System.out.println(b2);
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}
}
