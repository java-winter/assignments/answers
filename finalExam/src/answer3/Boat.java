package answer3;

public class Boat {
	private String ownerName;
    private int size;
    private int depth;
	
    public Boat(String ownerName, int size, int depth) {
		setOwnerName(ownerName);
		setSize(size);
		setDepth(depth);
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	@Override
	public String toString() {
	     return "Boat{" +
	                "ownerName='" + ownerName + '\'' +
	                ", size=" + size +
	                ", depth=" + depth +
	                '}';
	}
    
	
    
}
