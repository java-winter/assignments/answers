package answer4;

public class TextMessage extends Message {
	private String text;

	public TextMessage(User user, String text) {
		super(user);
		setText(text);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		if (text.contains("*") || text.contains("$") || text.contains("&")) {
			throw new IllegalArgumentException("message should not contain special characters");
		}
		if (text.length() > 500) {
			throw new IllegalArgumentException("message length should not exceed 500");
		}
		this.text = text;
	}

	@Override
	public void showMessage() {
		StringBuilder builder = new StringBuilder(super.getUser().toString());
		builder.append(", sends this message => " + text);
		System.out.println(builder.toString());
	}

}
