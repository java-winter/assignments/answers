package answer4;

public abstract class Message{
    private User user;
    
    public Message(User user){
    	setUser(user);
    }
    
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public abstract void showMessage ();

    @Override
    public String toString() {
        return "Message{" +
                "user=" + user +
                '}';
    }
}
