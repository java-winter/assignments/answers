package answer4;

public class App {

	public static void main(String[] args) {
		try {
          User user1 = new User("reza", "1234567897", "QC");
          Message message1 = new TextMessage(user1, "This is a message");
          message1.showMessage();

          User user2 = new User("sh", "1234567897", "QC");
          Message message2 = new TextMessage(user2, "This is a message");
          message2.showMessage();

          User user3 = new User("reza", "2222", "QC");
          Message message3 = new TextMessage(user3, "This is a message");
          message3.showMessage();

          User user4 = new User("reza", "1234567897", "Q");
          Message message4 = new TextMessage(user4, "This is a message");
          message4.showMessage();

          User user5 = new User("reza", "1234567897", "QC");
          Message message5 = new TextMessage(user5, "This is ** a message");
          message5.showMessage();

      }
      catch (IllegalArgumentException exc){
          System.out.println(exc.getMessage());
      }

		
	}

}
