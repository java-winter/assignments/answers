package answer4;

public class User {
    private String name;
    private String phoneNumber;
    private String province;

    public User(String name, String phoneNumber, String province) {
        setName(name);
        setPhoneNumber(phoneNumber);
        setProvince(province);
    }

    

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", province='" + province + '\'' +
                '}';
    }



	public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length() > 2 && name.length() < 100){
            this.name = name;
        }
        else{
            throw new IllegalArgumentException("name needs to be more than 2 characters");
        }
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if(phoneNumber.length() == 10){
            this.phoneNumber = phoneNumber;
        }
        else{
            throw new IllegalArgumentException("phone number needs to be 10 digits");
        }
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        if(province.length() == 2){
            this.province = province;
        }
        else{
            throw new IllegalArgumentException("province size needs to be 2");
        }
    }
}


